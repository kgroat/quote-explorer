# Quote Explorer

View the app at https://quotes.kgroat.dev/

This is a [T3 Stack](https://create.t3.gg/) project bootstrapped with `create-t3-app`.  It utilizes Next.js, Prisma, and Next Auth.

## Installing dependencies

The dependencies of this project are managed using [`pnpm`](https://pnpm.io/). To use it, I suggest enabling `corepack` with `corepack enable` and then run `corepack install` while in this directory -- this will install the correct version of `pnpm`.  **This is a one-time setup step**.

Once you have `pnpm`, you can run `pnpm install` to download all of the node modules that this project depends upon.

## Quick Start

Before beginning, you should copy the `.env.example` file and name the new copy `.env`.  Your local environment variables will go in this file.

This project depends upon a PostgreSQL database.  If you have one already running either locally or in the cloud, you can connect to it directly.  If not, and you have Docker installed and running, you can run `./start-database.sh` in your terminal to automatically set up a new database running in Docker.  Either way, you should place the connection string in a `.env` file under the variable `DATABASE_URL`.  If you used the `start-database.sh` script, the connection string should already have been populated, but verify that it is correct.

Once your `DATABASE_URL` is configured, you should run the command `pnpm run db:push`.  This will not only push the Prisma schema to your configured database, but it will also regenerate typescript types for the Prisma client to match the Schema.  These are important for making database queries and updates type-safe in your code.

The application also depends upon Discord authentication.  To set it up, go to [the Discord applications manager](https://discord.com/developers/applications) in the developer portal.  Sign in if you aren't already, and then create a new application.  Once you've created it, go to the OAuth2 section.  Start by adding a redirect pointed at `http://localhost:3000/api/auth/callback/discord`.  Next, copy your client id and secret.  Place these in your `.env` file as well, under the variables `DISCORD_CLIENT_ID` and `DISCORD_CLIENT_SECRET` respectively.

Once you've completed these steps, the application is fully configured, and you can start the dev server using `pnpm run dev` and visiting http://localhost:3000 in your browser.

## Deployment

Follow the create-t3-app deployment guides for [Vercel](https://create.t3.gg/en/deployment/vercel), [Netlify](https://create.t3.gg/en/deployment/netlify) and [Docker](https://create.t3.gg/en/deployment/docker) for more information.
