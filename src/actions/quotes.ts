'use server'

import { getServerAuthSession } from '~/server/auth'
import { db } from '~/server/db'

interface QuoteDetails {
  quote: string
  person: string
}

function cleanQuoteDetails (details: QuoteDetails) {
  details.quote = details.quote
    .replaceAll(/\s+/g, (spaces) => spaces.includes('\n') ? '\n' : ' ')
    .trim()

  details.person = details.person
    .replaceAll(/\s+/g, ' ')
    .trim()
}

function validateQuoteDetails (details: QuoteDetails) {
  cleanQuoteDetails(details)
  const { quote, person } = details

  if (quote.length < 10) {
    throw new Error('A quote must be at least 10 characters long')
  }

  if (!person) {
    throw new Error('You must supply a person who said the quote')
  }
}

export async function createQuote (details: QuoteDetails) {
  const session = await getServerAuthSession()

  if (!session) {
    throw new Error('You must be logged in to create a quote')
  }
  
  validateQuoteDetails(details)
  const { quote, person } = details

  const obj = await db.quote.create({
    data: {
      quote,
      person,
      createdById: session.user.id,
    },
  })

  return obj
}

export interface QuoteQuery {
  createdById?: string
  person?: string
}

export async function getAllQuotes (query: QuoteQuery = {}, skip = 0, providedTake = 10) {
  const take = Math.max(0, Math.min(providedTake, 25))
  const { createdById, person } = query

  const quotes = await db.quote.findMany({
    where: { createdById, person },
    orderBy: { createdAt: 'desc' },
    skip,
    take,
  })

  return quotes
}

export async function updateQuote (details: QuoteDetails & { id: number }) {
  const session = await getServerAuthSession()
  if (!session) {
    throw new Error('You must be logged in to modify a quote')
  }

  validateQuoteDetails(details)
  const { id, quote, person } = details

  const existing = await db.quote.findFirst({
    where: { id, createdById: session.user.id },
  })

  if (!existing) {
    throw new Error('You cannot modify a quote you did not create')
  }

  const data = await db.quote.update({
    where: { id },
    data: {
      quote,
      person,
    },
  })

  return data
}

export async function getQuoteCountByUser (userId: string) {
  const count = await db.quote.count({
    where: { createdById: userId },
  })

  return count
}

export async function getQuoteById (id: number) {
  const quote = await db.quote.findFirst({
    where: { id },
    select: {
      id: true,
      quote: true,
      person: true,
      createdBy: {
        select: {
          id: true,
          name: true,
        },
      },
    },
  })

  return quote
}

export type QuoteById = Awaited<ReturnType<typeof getQuoteById>>

export async function getRandomQuoteId () {
  const count = await db.quote.count()
  const skip = Math.floor(Math.random() * count)
  const selected = await db.quote.findFirstOrThrow({
    skip,
    select: { id: true },
  })

  return selected.id
}
