'use client'
import { signOut } from 'next-auth/react'

import styles from './index.module.css'

export default function SignoutClientPage () {
  return (
    <div className='centeredPage'>
      Are you sure you want to sign out?
      <button className={styles.signoutButton} onClick={() => signOut()}>Sign out</button>
    </div>
  )
}
