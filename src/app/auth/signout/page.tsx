import { redirect } from 'next/navigation'

import { getServerAuthSession } from '~/server/auth'

import SignoutClientPage from './client'

export default async function SigninPage () {
  const session = await getServerAuthSession()

  if (!session) {
    return redirect('/')
  }

  return <SignoutClientPage />
}
