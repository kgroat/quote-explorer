import { redirect } from 'next/navigation'
import { getProviders } from 'next-auth/react'

import { getServerAuthSession } from '~/server/auth'

import LoginClientPage from './client'

export default async function SigninPage () {
  const session = await getServerAuthSession()

  if (session) {
    return redirect('/')
  }

  const providers = (await getProviders())!
  return <LoginClientPage providers={providers} />
}
