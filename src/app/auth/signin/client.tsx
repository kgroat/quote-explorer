'use client'
import { type getProviders, signIn } from 'next-auth/react'

import styles from './index.module.css'

interface Props {
  providers: Awaited<ReturnType<typeof getProviders>>
}

export default function SigninClientPage ({ providers }: Props) {
  if (!providers) {
    return <div className='centeredPage'>No providers registered.</div>
  }

  return (
    <div className='centeredPage'>
      {Object.values(providers).map((provider) => (
        <button key={provider.name} className={styles.signinButton} onClick={() => signIn(provider.id)}>
          Sign in with {provider.name}
        </button>
      ))}
    </div>
  )
}
