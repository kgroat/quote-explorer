export default function NotFound () {
  return (
    <div className='centeredPage'>
      <h1>404</h1>
      <h2>Not Found</h2>
    </div>
  )
}
