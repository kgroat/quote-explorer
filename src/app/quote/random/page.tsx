'use client'
import { useQuery } from '@tanstack/react-query'
import { redirect } from 'next/navigation'
import { useEffect } from 'react'

import { getRandomQuoteId } from '~/actions/quotes'
import Spinner from '~/components/Spinner'

export default function RandomQuotePage () {
  const { data: quoteId, error } = useQuery({
    queryKey: ['randomQuoteId'],
    queryFn: () => getRandomQuoteId(),
  })

  useEffect(() => {
    if (quoteId) {
      redirect(`/quote/${quoteId}`)
    }
  }, [quoteId])

  if (error) {
    return (
      <div className='centeredPage'>
        Something went wrong, please try again later.
      </div>
    )
  }

  return (
    <div className='centeredPage'>
      <Spinner />
    </div>
  )
}
