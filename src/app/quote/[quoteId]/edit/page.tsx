import { getQuoteById } from '~/actions/quotes'

import EditQuoteClientPage from './client'

export default async function EditQuotePage ({ params }: { params: { quoteId: string } }) {
  const quoteId = parseInt(params.quoteId)

  if (Number.isNaN(quoteId)) {
    return <EditQuoteClientPage quote={null} />
  }

  const quote = await getQuoteById(quoteId)

  return <EditQuoteClientPage quote={quote} />
}
