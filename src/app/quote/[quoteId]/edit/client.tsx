'use client'

import { useMutation } from '@tanstack/react-query'
import { notFound, useRouter } from 'next/navigation'
import { useState } from 'react'

import { type QuoteById, updateQuote } from '~/actions/quotes'
import FloatingLabelInput from '~/components/FloatingLabelInput'
import Spinner from '~/components/Spinner'
import TextArea from '~/components/TextArea'

interface Props {
  quote: QuoteById
}

export default function EditQuoteClientPage ({ quote: quoteObj }: Props) {
  const [quote, setQuote] = useState(quoteObj?.quote ?? '')
  const [person, setPerson] = useState(quoteObj?.person ?? '')
  const router = useRouter()
  const { mutateAsync, isPending, isSuccess, error } = useMutation({
    mutationFn: updateQuote,
  })

  if (!quote) {
    return notFound()
  }

  async function onSubmit (ev: React.FormEvent<HTMLFormElement>) {
    ev.preventDefault()

    const result = await mutateAsync({
      id: quoteObj!.id,
      quote,
      person,
    })

    router.push(`/quote/${result.id}?refresh=true`)
  }

  if (isSuccess) {
    return (
      <div className='centeredPage'>
        Success!
      </div>
    )
  }

  if (isPending) {
    return (
      <div className='centeredPage'>
        <Spinner />
      </div>
    )
  }

  return (
    <form className='centeredPage' onSubmit={onSubmit}>
      <TextArea
        label='Quote'
        name='quote'
        rows={5}
        value={quote}
        onChange={ev => setQuote(ev.currentTarget.value)}
      />
      <FloatingLabelInput
        label='Person'
        name='person'
        value={person}
        onChange={ev => setPerson(ev.currentTarget.value)}
        errors={error ? [error.message] : undefined}
      />
      <button type='submit'>Submit</button>
    </form>
  )
}
