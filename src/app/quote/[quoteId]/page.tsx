import { getQuoteById } from '~/actions/quotes'

import QuoteClientPage from './client'

export default async function QuoteByIdPage ({ params }: { params: { quoteId: string } }) {
  const quoteId = parseInt(params.quoteId)

  if (Number.isNaN(quoteId)) {
    return <QuoteClientPage quote={null} />
  }

  const quote = await getQuoteById(quoteId)

  return <QuoteClientPage quote={quote} />
}
