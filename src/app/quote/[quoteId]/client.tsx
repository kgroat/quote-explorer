'use client'

import Link from 'next/link'
import { notFound, useRouter, useSearchParams } from 'next/navigation'
import { useSession } from 'next-auth/react'
import { useEffect } from 'react'
import { LuPencil } from 'react-icons/lu'

import { type QuoteById } from '~/actions/quotes'

import styles from './index.module.css'

interface Props {
  quote: QuoteById
}

function EditButton ({ quote }: Props) {
  const session = useSession()

  if (session.status !== 'authenticated') {
    return null
  }

  if (session.data.user.id === quote!.createdBy.id) {
    return (
      <Link className={styles.editIcon} href={`/quote/${quote!.id}/edit`}><LuPencil /></Link>
    )
  }
}

export default function QuoteByIdClientPage ({ quote }: Props) {
  const router = useRouter()
  const query = useSearchParams()
  const refresh = query.get('refresh') === 'true'

  useEffect(() => {
    if (refresh){
      router.refresh()
    }
  }, [router, refresh])

  if (!quote) {
    return notFound()
  }

  const quoteParagraphs = quote.quote.split('\n').map((paragraph, idx) => (
    <div key={idx}>{paragraph}</div>
  ))

  return (
    <div className={`centeredPage ${styles.quoteBlock}`}>
      <EditButton quote={quote} />
      <h1 className={styles.quote}>{quoteParagraphs}</h1>
      <h2 className={styles.person}>
        --&nbsp;<Link href={`/quote?person=${quote.person}`}>{quote.person}</Link>
      </h2>
      <p>
        Submitted by <Link href={`/user/${quote.createdBy.id}`}>{quote.createdBy.name}</Link>
      </p>
    </div>
  )
}
