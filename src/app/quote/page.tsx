'use client'

import { useInfiniteQuery } from '@tanstack/react-query'
import Error from 'next/error'
import Link from 'next/link'
import { useSearchParams } from 'next/navigation'

import { getAllQuotes, type QuoteQuery } from '~/actions/quotes'
import Spinner from '~/components/Spinner'

import styles from './index.module.css'

const PAGE_SIZE = 10
export default function AllQuotesClientPage () {
  const searchParams = useSearchParams()
  const person = searchParams.get('person') ?? undefined
  const createdById = searchParams.get('userId') ?? undefined
  const qurey: QuoteQuery = { person, createdById }

  const { isFetchingNextPage, hasNextPage, data, error, fetchNextPage } = useInfiniteQuery({
    queryKey: ['all-quotes', qurey],
    queryFn: ({ pageParam: skip }) => getAllQuotes(qurey, skip, PAGE_SIZE),
    getNextPageParam: (lastPage, _pages, lastPageParam) => {
      if (lastPage.length < PAGE_SIZE) {
        return null
      }

      return lastPageParam + PAGE_SIZE
    },
    initialPageParam: 0,
  })

  if (error) {
    return (
      <Error statusCode={500} />
    )
  }

  if (!data) {
    return (
      <div className='centeredPage'>
        <Spinner />
      </div>
    )
  }

  const quotes = data.pages.flat().map(({ id, quote, person }) => (
    <li key={id} className={styles.quote}>
      <Link href={`/quote/${id}`} className={styles.quoteText}>{quote}</Link>
      &nbsp;--&nbsp;
      <Link href={`/quote?person=${person}`}>{person}</Link>
    </li>
  ))

  return (
    <div className='listPage'>
      <ul className={styles.quotes}>
        {quotes}
      </ul>
      {
        isFetchingNextPage
          ? <Spinner className={styles.nextPageSpinner} />
          : hasNextPage && (
           <button className={styles.loadMore} onClick={() => fetchNextPage()}>Load more...</button>
          )
      }
    </div>
  )
}
