'use client'

import { useMutation } from '@tanstack/react-query'
import { useRouter } from 'next/navigation'
import { useState } from 'react'

import { createQuote } from '~/actions/quotes'
import FloatingLabelInput from '~/components/FloatingLabelInput'
import Spinner from '~/components/Spinner'
import TextArea from '~/components/TextArea'

export default function CreateQuotePage () {
  const [quote, setQuote] = useState('')
  const [person, setPerson] = useState('')
  const router = useRouter()
  const { mutateAsync, isPending, isSuccess, error } = useMutation({
    mutationFn: createQuote,
  })

  async function onSubmit (ev: React.FormEvent<HTMLFormElement>) {
    ev.preventDefault()
    const result = await mutateAsync({
      quote,
      person,
    })

    router.push(`/quote/${result.id}`)
  }

  if (isSuccess) {
    return (
      <div className='centeredPage'>
        Success!
      </div>
    )
  }

  if (isPending) {
    return (
      <div className='centeredPage'>
        <Spinner />
      </div>
    )
  }

  return (
    <form className='centeredPage' onSubmit={onSubmit}>
      <TextArea
        label='Quote'
        name='quote'
        rows={5}
        value={quote}
        onChange={ev => setQuote(ev.currentTarget.value)}
      />
      <FloatingLabelInput
        label='Person'
        name='person'
        value={person}
        onChange={ev => setPerson(ev.currentTarget.value)}
        errors={error ? [error.message] : undefined}
      />
      <button type='submit'>Submit</button>
    </form>
  )
}
