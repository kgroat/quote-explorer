import Link from 'next/link'

import { getServerAuthSession } from '~/server/auth'

import styles from './index.module.css'

export default async function Home() {
  const session = await getServerAuthSession()

  return (
    <div className='centeredPage'>
      <h1 className={styles.center}>Quote Explorer</h1>
      <p className={styles.center}>A website devoted to submitting and viewing famous quotes</p>
      <p><Link href='/quote/random' className={styles.center}>View a random quote</Link></p>
      <p><Link href='/quote' className={styles.center}>View all of the latest quotes</Link></p>
      {session && <p><Link href='/quote/create' className={styles.center}>Submit your own quote</Link></p>}
    </div>
  )
}
