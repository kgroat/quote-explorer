import { getQuoteCountByUser } from '~/actions/quotes'
import { db } from '~/server/db'

import UserClientPage from './client'

export default async function QuoteByIdPage ({ params }: { params: { userId: string } }) {
  const { userId } = params

  if (!userId) {
    return <UserClientPage user={null} count={0} />
  }

  const user = await db.user.findFirst({
    where: {
      id: userId,
    },
  })

  const count = await getQuoteCountByUser(userId)

  return <UserClientPage user={user} count={count} />
}
