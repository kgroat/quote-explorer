'use client'

import { type User } from '@prisma/client'
import Link from 'next/link'
import { notFound } from 'next/navigation'

import styles from './index.module.css'

interface Props {
  user: User | null
  count: number
}

export default function UserClientPage ({ user, count }: Props) {
  if (!user) {
    return notFound()
  }

  return (
    <div className={styles.user}>
      {
        /* Can't use `Image` because the src image is external */
        /* eslint-disable-next-line @next/next/no-img-element */
        user.image && <img
          className={styles.avatar}
          src={user.image}
          alt={`${user.name ?? 'User'}'s avatar`}
        />
      }
      <h1 className={styles.name}>{user.name ?? user.email}</h1>
      <p>Joined {user.createdAt.toLocaleDateString('en-us', { day: 'numeric', month: 'numeric', year: 'numeric' })}</p>
      <Link href={`/quote?userId=${user.id}`}>See all {count} quotes submitted by {user.name}</Link>
    </div>
  )
}
