import '~/styles/globals.css'
import '~/registerServiceWorker'


import { Analytics } from '@vercel/analytics/react'
import { SpeedInsights } from '@vercel/speed-insights/next'
import { type Metadata, type Viewport } from 'next'
import { Inter } from 'next/font/google'
import { type StaticImageData } from 'next/image'

import GitLab from '~/assets/gitlab.svg'
import TopNav from '~/components/TopNav'
import Providers from '~/helpers/Providers'

import styles from './layout.module.css'

const inter = Inter({
  subsets: ['latin'],
})

export const metadata: Metadata = {
  title: 'Quote Explorer',
  description: 'An app to explore quotes from famous people',
  icons: [{ rel: 'icon', url: '/favicon.ico' }],
  manifest: '/manifest.json',
}

export const viewport: Viewport = {
  themeColor: '#1d062d',
  initialScale: 1,
  maximumScale: 1,
  userScalable: false,
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang='en'>
      <body className={inter.className}>
        <Analytics />
        <SpeedInsights />
        <Providers>
          <TopNav  />
          <main className={styles.main}>
            {children}
          </main>
          <a className={styles.gitlabLink} href='https://gitlab.com/kgroat/quote-explorer' target='_blank'>
            {
              // eslint-disable-next-line @next/next/no-img-element
              <img src={(GitLab as StaticImageData).src} className={styles.gitlabLogo} alt='GitLab Repo' />
            }
          </a>
        </Providers>
      </body>
    </html>
  )
}
