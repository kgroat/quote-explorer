'use client'

import { env } from '~/env'

async function register () {
  if (env.NEXT_PUBLIC_SKIP_SW) return

  if ('serviceWorker' in navigator) {
    await navigator.serviceWorker.register('/service-worker.js')
  }
}

register()
  .catch((err) => {
    console.error('Failed to register service worker:', err)
  })
