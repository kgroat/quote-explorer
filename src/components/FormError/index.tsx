import styles from './index.module.css'

export default function FormError ({ children }: { children: React.ReactNode }) {
  return (
    <div className={styles.formError} data-testid='form-error'>
      {children}
    </div>
  )
}
