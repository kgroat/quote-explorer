'use client'

import Link from 'next/link'
import { useSession } from 'next-auth/react'

import Spinner from '../Spinner'

import styles from './index.module.css'

function CreateLink () {
  const session = useSession()

  if (session.status !== 'authenticated') {
    return null
  }

  return <li><Link href='/quote/create'>Create</Link></li>
}

function AuthStatus () {
  const session = useSession()

  if (session.status === 'loading') {
    return <Spinner className={styles.sessionSpinner} size={24} />
  }


  if (session.status === 'unauthenticated') {
    return <Link href='/auth/signin'>Sign in</Link>
  }

  return (
    <>
      <span className={styles.greeting}>Hello, <Link href={`/user/${session.data!.user.id}`}>{session.data!.user.name}</Link>!</span>
      <Link href='/auth/signout'>Sign out</Link>
    </>
  )
}

export default function TopNav () {
  return (
    <nav className={styles.topNav}>
      <ul className={styles.navList} role='navigation'>
        <li><Link href='/'>Home</Link></li>
        <li><Link href='/quote/random'>Random</Link></li>
        <li><Link href='/quote'>Latest</Link></li>
        <CreateLink />
      </ul>
      <div className={styles.grow} />
      <div>
        <AuthStatus />
      </div>
    </nav>
  )
}
