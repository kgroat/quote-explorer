import React from 'react'

import styles from './index.module.css'

export interface SpinnerProps {
  size?: number | string

  className?: string
  style?: React.CSSProperties
}

const ARM_COUNT = 12

const SpinnerArm = ({ index }: { index: number }) => (
  <div className={styles.armWrapper} style={{ transform: `rotate(${-360 * index / ARM_COUNT}deg)` }}>
    <div className={styles.arm} style={{ animationDelay: `${-index / ARM_COUNT}s` }} />
  </div>
)

const Spinner = (props: SpinnerProps) => {
  const { className, size, style } = props

  let finalStyle: React.CSSProperties | undefined = style
  if (size) {
    finalStyle = {
      ...finalStyle,
      width: size,
      height: size,
    }
  }

  return (
    <div className={`${styles.spinner} ${className}`} style={finalStyle}>
      {Array.from(Array(ARM_COUNT)).map((_, index) => <SpinnerArm key={index} index={index} />)}
    </div>
  )
}

export default Spinner
