import styles from './index.module.css'

interface Props {
  label: string
  name: string
  rows?: number
  value?: string
  onChange?: (ev: React.ChangeEvent<HTMLTextAreaElement>) => void
  // errors?: string[]
  required?: boolean
  'data-testid'?: string
}

export default function TextArea (props: Props) {
  const {
    label,
    name,
    rows = 3,
    value,
    onChange,
    required,
    'data-testid': testid,
  } = props

  return (
    <label className={styles.label}>
      <textarea
        className={styles.input}
        name={name}
        placeholder=' '
        value={value}
        onChange={onChange}
        required={required}
        data-testid={testid}
        rows={rows}
      />
      <span className={styles.text}>{label}</span>
    </label>
  )
}
