import FormError from '../FormError'

import styles from './index.module.css'

interface Props {
  label: string
  name: string
  type?: React.HTMLInputTypeAttribute
  value?: string
  onChange?: (ev: React.ChangeEvent<HTMLInputElement>) => void
  errors?: string[]
  required?: boolean
  autoComplete?: string
  'data-testid'?: string
  children?: React.ReactNode
}

export default function FloatingLabelInput (props: Props) {
  const {
    label,
    name,
    type = 'text',
    value,
    onChange,
    errors,
    required,
    autoComplete = 'off',
    'data-testid': testid,
    children,
  } = props

  return (
    <>
      <label className={styles.label}>
        <input
          className={styles.input}
          name={name}
          type={type}
          placeholder=' '
          value={value}
          onChange={onChange}
          required={required}
          autoComplete={autoComplete}
          data-testid={testid}
        />
        <span className={styles.text}>{label}</span>
        {children}
      </label>
      {errors?.map((err, idx) => <FormError key={idx}>{err}</FormError>)}
    </>
  )
}
