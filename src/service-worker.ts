/// <reference lib="WebWorker" />
/// <reference types="workbox-sw" />

importScripts(
  'https://storage.googleapis.com/workbox-cdn/releases/6.4.1/workbox-sw.js'
)

workbox.routing.registerRoute(
  ({request}) => {
    if (!request) return false

    const url = new URL(request.url)

    if (url.hostname !== location.hostname) {
      if (request.destination === 'image') return true

      return false
    }

    if (request.destination === 'document') return true
    if (request.destination === 'script') return true
    if (request.destination === 'style') return true
    if (request.destination === 'image') return true

    return false
  },
  new workbox.strategies.StaleWhileRevalidate()
)
