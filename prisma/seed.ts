import { PrismaClient } from '@prisma/client'

const db = new PrismaClient({
  log: ['info', 'query', 'warn', 'error'],
})

async function main () {
  await db.$transaction(async (tx) => {
    const user = await tx.user.create({
      data: {
        name: 'kaiser_kevin',
        email: 'kgroat09@gmail.com',
        image: 'https://cdn.discordapp.com/avatars/316632060070461440/f8c07e7c673b9274bce5d8195ccf94ce.png',
        createdAt: '2024-03-01T01:42:07.173Z',
      },
    })
  
    await tx.account.create({
      data: {
        userId: user.id,
        type: 'oauth',
        provider: 'discord',
        providerAccountId: '316632060070461440',
      },
    })
  
    const quoteData = [
      ['God does not play dice with the universe.','Albert Einstein','2024-03-01T01:42:57.149Z','2024-03-01T01:42:57.149Z'],
      ['You must be the change you wish to see in the world.','Mahatma Gandhi','2024-03-01T01:50:52.816Z','2024-03-01T01:50:52.816Z'],
      ['In the end, it\'s not the years in your life that count. It\'s the life in your years.','Abraham Lincoln','2024-03-01T01:51:38.879Z','2024-03-01T01:51:38.879Z'],
      ['Life is trying things to see if they work.','Ray Bradbury','2024-03-01T01:53:01.011Z','2024-03-01T01:53:01.011Z'],
      ['Success is not final; failure is not fatal: It is the courage to continue that counts.','Winston Churchill','2024-03-01T01:53:58.104Z','2024-03-01T01:53:58.104Z'],
      ['Leave nothing for tomorrow which can be done today.','Abraham Lincoln','2024-03-01T01:54:24.248Z','2024-03-01T01:54:24.248Z'],
      ['An apple a day keeps the doctor away.','Benjamin Franklin','2024-03-01T01:55:05.803Z','2024-03-01T01:55:05.803Z'],
      ['How wonderful it is that nobody need wait a single moment before starting to improve the world.','Anne Frank','2024-03-01T01:59:38.814Z','2024-03-01T01:59:38.814Z'],
      ['It does not matter how slowly you go as long as you do not stop.','Confucius','2024-03-01T02:00:19.182Z','2024-03-01T02:00:19.182Z'],
      ['With great power comes great responsibility.','Stan Lee','2024-03-01T02:00:46.028Z','2024-03-01T02:00:46.028Z'],
      ['I would rather die of passion than of boredom.','Vincent van Gogh','2024-03-01T02:01:13.64Z','2024-03-01T02:01:13.64Z'],
      ['Be yourself; everyone else is already taken.','Oscar Wilde','2024-03-01T02:05:33.952Z','2024-03-01T02:05:33.952Z'],
      ['Two things are infinite: the universe and human stupidity; and I\'m not sure about the universe.','Albert Einstein','2024-03-01T02:06:08.8Z','2024-03-01T02:06:08.8Z'],
      ['You know you\'re in love when you can\'t fall asleep because reality is finally better than your dreams.','Dr. Seuss','2024-03-01T02:06:37.213Z','2024-03-01T02:06:37.213Z'],
      ['You only live once, but if you do it right, once is enough.','Mae West','2024-03-01T02:07:16.921Z','2024-03-01T02:07:16.921Z'],
      ['In three words I can sum up everything I\'ve learned about life: it goes on.','Robert Frost','2024-03-01T02:07:39.07Z','2024-03-01T02:07:39.07Z'],
      ['Learning never exhausts the mind.','Leonardo da Vinci','2024-03-02T01:57:19.776Z','2024-03-02T01:57:19.776Z'],
      ['There is no charm equal to tenderness of heart.','Jane Austen','2024-03-02T01:57:50.666Z','2024-03-02T01:57:50.666Z'],
      ['All that we see or seem is but a dream within a dream.','Edgar Allan Poe','2024-03-02T01:58:15.481Z','2024-03-02T01:58:15.481Z'],
      ['No act of kindness, no matter how small, is ever wasted.','Aesop','2024-03-02T01:59:02.471Z','2024-03-02T01:59:02.471Z'],
      ['Keep your face always toward the sunshine - and shadows will fall behind you.','Walt Whitman','2024-03-02T02:00:32.231Z','2024-03-02T02:00:32.231Z'],
      ['Being entirely honest with oneself is a good exercise.','Sigmund Freud','2024-03-02T02:00:46.8Z','2024-03-02T02:00:46.8Z'],
      ['The journey of a thousand miles begins with one step.','Lao Tzu','2024-03-02T02:01:30.92Z','2024-03-02T02:01:30.92Z'],
      ['Not all those who wander are lost.','J. R. R. Tolkien','2024-03-02T02:04:07.844Z','2024-03-02T02:04:07.844Z'],
      ['Tell me and I forget. Teach me and I remember. Involve me and I learn.','Benjamin Franklin','2024-03-02T02:04:47.425Z','2024-03-02T02:04:47.425Z'],
      ['If opportunity doesn\'t knock, build a door.','Milton Berle','2024-03-02T02:05:46.652Z','2024-03-02T02:05:46.652Z'],
      ['We know what we are, but know not what we may be.','William Shakespeare','2024-03-02T02:33:13.242Z','2024-03-02T02:33:13.242Z'],
      ['It\'s not what you look at that matters, it\'s what you see.','Henry David Thoreau','2024-03-02T02:33:52.923Z','2024-03-02T02:33:52.923Z'],
      ['Life\'s most persistent and urgent question is, \'What are you doing for others?\'','Martin Luther King, Jr.','2024-03-02T02:35:43.824Z','2024-03-02T02:35:43.824Z'],
      ['Believe you can and you\'re halfway there.','Theodore Roosevelt','2024-03-02T02:36:11.295Z','2024-03-02T02:36:11.295Z'],
      ['Education is the most powerful weapon which you can use to change the world.','Nelson Mandela','2024-03-02T02:37:20.479Z','2024-03-02T02:37:20.479Z'],
      ['If you want to live a happy life, tie it to a goal, not to people or things.','Albert Einstein','2024-03-02T19:49:57.033Z','2024-03-02T19:49:57.033Z'],
      ['Life is not a problem to be solved, but a reality to be experienced.','Soren Kierkegaard','2024-03-02T19:50:49.879Z','2024-03-02T19:50:49.879Z'],
      ['The unexamined life is not worth living.','Socrates','2024-03-02T19:51:06.803Z','2024-03-02T19:51:06.803Z'],
      ['Turn your wounds into wisdom.','Oprah Winfrey','2024-03-02T19:51:23.506Z','2024-03-02T19:51:23.506Z'],
      ['The way I see it, if you want the rainbow, you gotta put up with the rain.','Dolly Parton','2024-03-02T19:53:18.578Z','2024-03-02T19:53:18.578Z'],
      ['Life’s tragedy is that we get old too soon and wise too late.','Benjamin Franklin','2024-03-02T19:55:50.211Z','2024-03-02T19:55:50.211Z'],
      ['The best way to predict your future is to create it.','Abraham Lincoln','2024-03-02T19:58:49.638Z','2024-03-02T19:58:49.638Z'],
    ] as [string, string, string, string][]
  
    await tx.quote.createMany({
      data: quoteData.map(([quote, person, createdAt, updatedAt]) => ({
        quote,
        person,
        createdAt,
        updatedAt,
        createdById: user.id,
      })),
    })
  })
}

main()
  .then(async () => {
    await db.$disconnect()
  })
  .catch(async (err) => {
    console.error(err)
    await db.$disconnect()
    process.exit(1)
  })
