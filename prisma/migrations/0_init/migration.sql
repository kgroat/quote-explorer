-- CreateTable
CREATE TABLE "qe_quote" (
    "id" SERIAL NOT NULL,
    "quote" TEXT NOT NULL,
    "person" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "createdById" TEXT NOT NULL,

    CONSTRAINT "qe_quote_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "qe_account" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "provider" TEXT NOT NULL,
    "providerAccountId" TEXT NOT NULL,
    "refresh_token" TEXT,
    "access_token" TEXT,
    "expires_at" INTEGER,
    "token_type" TEXT,
    "scope" TEXT,
    "id_token" TEXT,
    "session_state" TEXT,
    "refresh_token_expires_in" INTEGER,
    "created_at" INTEGER,

    CONSTRAINT "qe_account_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "qe_session" (
    "id" TEXT NOT NULL,
    "sessionToken" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "expires" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "qe_session_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "qe_user" (
    "id" TEXT NOT NULL,
    "name" TEXT,
    "email" TEXT,
    "emailVerified" TIMESTAMP(3),
    "image" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "qe_user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "qe_verification_token" (
    "identifier" TEXT NOT NULL,
    "token" TEXT NOT NULL,
    "expires" TIMESTAMP(3) NOT NULL
);

-- CreateIndex
CREATE INDEX "qe_quote_person_idx" ON "qe_quote"("person");

-- CreateIndex
CREATE UNIQUE INDEX "qe_account_provider_providerAccountId_key" ON "qe_account"("provider", "providerAccountId");

-- CreateIndex
CREATE UNIQUE INDEX "qe_session_sessionToken_key" ON "qe_session"("sessionToken");

-- CreateIndex
CREATE UNIQUE INDEX "qe_user_email_key" ON "qe_user"("email");

-- CreateIndex
CREATE UNIQUE INDEX "qe_verification_token_token_key" ON "qe_verification_token"("token");

-- CreateIndex
CREATE UNIQUE INDEX "qe_verification_token_identifier_token_key" ON "qe_verification_token"("identifier", "token");

-- AddForeignKey
ALTER TABLE "qe_quote" ADD CONSTRAINT "qe_quote_createdById_fkey" FOREIGN KEY ("createdById") REFERENCES "qe_user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "qe_account" ADD CONSTRAINT "qe_account_userId_fkey" FOREIGN KEY ("userId") REFERENCES "qe_user"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "qe_session" ADD CONSTRAINT "qe_session_userId_fkey" FOREIGN KEY ("userId") REFERENCES "qe_user"("id") ON DELETE CASCADE ON UPDATE CASCADE;
